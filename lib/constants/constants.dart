const String GTFS_VERSION = "20220413-1";
const String APP_VERSION = "1.0.5";
const String LAST_UPDATED = "2022-04-15";
const String CITY_WEBSITE = "https://www.szekesfehervar.hu/buszmenetrend";
const String VOLANBUSZ_WEBSITE = "https://www.volanbusz.hu/hu/menetrendek/helyi-jaratok/szekesfehervar";
