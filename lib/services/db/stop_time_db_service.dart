import 'package:menetrend_szekesfehervar/models/stop_time_model.dart';
import 'package:menetrend_szekesfehervar/services/db/db_helper.dart';
import 'package:sqflite/sqflite.dart';

class StopTimeDbService {

  Future<List<StopTimeModel>> queryStopTimesForTrip(String tripId) async {
    final Database db = await DbHelper().initializeDb();
    final String queryString =
        "Select StopTimes.stop_id, stop_name, arrival_time, stops.stop_lat, stops.stop_lon"
        " from trips"
        " inner join StopTimes on Trips.trip_id = stopTimes.trip_id"
        " inner join stops on stops.stop_id = StopTimes.stop_id"
        " where Trips.trip_id = \"" + tripId + "\""
        " order by arrival_time asc";
    final List<Map<String, Object?>> queryResult = await db.rawQuery(queryString);
    return queryResult.map((e) => StopTimeModel.fromMap(e)).toList();
  }

}