import 'dart:io';

import 'package:menetrend_szekesfehervar/constants/constants.dart' as Constants;
import 'package:path/path.dart';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

class DbHelper {

  static const String GTFS_VERSION_KEY = "GTFS_VERSION";

  Future<Database> initializeDb() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "menetrend.db");

    // Check if the database exists
    var exists = await databaseExists(path);
    var outdated = await databaseOutdated();

    if (!exists || outdated) {
      // Should happen only the first time you launch your application
      print("Creating new copy from asset");

      // Make sure the parent directory exists
      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (_) {}

      // Copy from asset
      ByteData data = await rootBundle.load(join("assets/db", "menetrend.db"));
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      // Write and flush the bytes written
      await File(path).writeAsBytes(bytes, flush: true);
      await saveGTFSVersion();

    } else {
      print("Opening existing database");
    }
    // open the database
    return openDatabase(path, readOnly: true);
  }

  Future<bool> databaseOutdated() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    String? GTFSVersion = sharedPreferences.getString(GTFS_VERSION_KEY);
    if (GTFSVersion != null && GTFSVersion == Constants.GTFS_VERSION) {
      return false;
    }
    return true;
  }

  Future<void> saveGTFSVersion() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(GTFS_VERSION_KEY, Constants.GTFS_VERSION);
  }

}