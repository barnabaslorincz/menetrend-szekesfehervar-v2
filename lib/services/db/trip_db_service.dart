import 'package:intl/intl.dart';
import 'package:menetrend_szekesfehervar/models/trip_model.dart';
import 'package:menetrend_szekesfehervar/models/trip_with_route_model.dart';
import 'package:menetrend_szekesfehervar/services/db/db_helper.dart';
import 'package:sqflite/sqflite.dart';

class TripDbService {

  Future<List<TripModel>> queryTripsForRouteWithDate(String routeId, DateTime date) async {
    final Database db = await DbHelper().initializeDb();
    final DateFormat dateFormat = new DateFormat('yyyMMdd');
    final DateFormat dayFormat = new DateFormat('EEEE');
    final String formattedDate = dateFormat.format(date);
    final String formattedDay = dayFormat.format(date);
    final String queryString = "SELECT Trips.trip_id,Trips.trip_headsign,Trips.shape_id,StopTimes.departure_time"
        " from trips"
        " left join CalendarDates on trips.service_id = CalendarDates.service_id"
        " and CalendarDates.date = " + formattedDate + ""
        " inner join calendar on calendar.service_id = trips.service_id"
        " inner join StopTimes on StopTimes.trip_id = trips.trip_id"
        " where StopTimes.stop_sequence = 0"
        " and trips.route_id=\"" + routeId + "\""
        " and Calendar.start_date <= " + formattedDate + ""
        " and Calendar.end_date >= " + formattedDate + ""
        " and ((calendar." + formattedDay + " = 1"
        " and CalendarDates.exception_type is null)"
        " or CalendarDates.exception_type = 1)"
        " order by StopTimes.departure_time ASC";
    final List<Map<String, Object?>> queryResult = await db.rawQuery(queryString);
    return queryResult.map((e) => TripModel.fromMap(e)).toList();
  }

  Future<List<TripModelWithRoute>> queryTripsForStopNameWithDate(String stopName, DateTime date) async {
    final Database db = await DbHelper().initializeDb();
    final DateFormat dateFormat = new DateFormat('yyyMMdd');
    final String formattedDate = dateFormat.format(date);
    final String queryString = "SELECT routes.route_short_name, Trips.trip_id,Trips.trip_headsign, StopTimes.departure_time"
        " from trips"
        " left join CalendarDates on trips.service_id = CalendarDates.service_id and CalendarDates.date = " + formattedDate + ""
        " inner join calendar on calendar.service_id = trips.service_id"
        " inner join StopTimes on StopTimes.trip_id = trips.trip_id"
        " inner join routes on routes.route_id = trips.route_id"
        " left join Stops on  StopTimes.stop_id = Stops.stop_id"
        " where Stops.stop_name = \"" + stopName + "\" and Calendar.start_date <= " + formattedDate + " and Calendar.end_date >= " + formattedDate + " and ((calendar.tuesday = 1 and CalendarDates.exception_type is null) or CalendarDates.exception_type = 1)"
        " order by StopTimes.departure_time ASC;";
    final List<Map<String, Object?>> queryResult = await db.rawQuery(queryString);
    return queryResult.map((e) => TripModelWithRoute.fromMap(e)).toList();
  }

  Future<List<TripModelWithRoute>> queryTripsForStopIdWithDate(String stopId, DateTime date) async {
    final Database db = await DbHelper().initializeDb();
    final DateFormat dateFormat = new DateFormat('yyyMMdd');
    final String formattedDate = dateFormat.format(date);
    final String queryString = "SELECT routes.route_short_name, Trips.trip_id,Trips.trip_headsign, StopTimes.departure_time"
        " from trips"
        " left join CalendarDates on trips.service_id = CalendarDates.service_id and CalendarDates.date = " + formattedDate + ""
        " inner join calendar on calendar.service_id = trips.service_id"
        " inner join StopTimes on StopTimes.trip_id = trips.trip_id"
        " inner join routes on routes.route_id = trips.route_id"
        " where StopTimes.stop_id = \"" + stopId + "\" and Calendar.start_date <= " + formattedDate + " and Calendar.end_date >= " + formattedDate + " and ((calendar.tuesday = 1 and CalendarDates.exception_type is null) or CalendarDates.exception_type = 1)"
        " order by StopTimes.departure_time ASC;";
    final List<Map<String, Object?>> queryResult = await db.rawQuery(queryString);
    return queryResult.map((e) => TripModelWithRoute.fromMap(e)).toList();
  }

}