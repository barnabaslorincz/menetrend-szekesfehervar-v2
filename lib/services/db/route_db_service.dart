import 'package:menetrend_szekesfehervar/models/route_model.dart';
import 'package:menetrend_szekesfehervar/services/db/db_helper.dart';
import 'package:menetrend_szekesfehervar/services/favourites/favourites_service.dart';
import 'package:sqflite/sqflite.dart';

class RouteDbService {

  final FavouritesService favouritesService = FavouritesService();

  Future<List<RouteModel>> queryRoutes({String? searchText}) async {
    final Database db = await DbHelper().initializeDb();
    final String queryString =
        "SELECT * FROM Routes ORDER BY CAST(route_short_name AS INTEGER)";
    final List<Map<String, Object?>> queryResult = await db.rawQuery(queryString);
    List<RouteModel> routeModelList = queryResult.map((e) => RouteModel.fromMap(e)).toList();
    final List<String> favouriteRouteIds = await favouritesService.getFavouriteRouteIds();
    if (favouriteRouteIds.isNotEmpty) {
      List<RouteModel> favouriteRouteModelList = [];
      List<RouteModel> nonFavouriteRouteModelList = [];
      routeModelList.forEach((routeModel) {
        if (favouriteRouteIds.contains(routeModel.routeId)) {
          routeModel.isFavourite = true;
          favouriteRouteModelList.add(routeModel);
        } else {
          nonFavouriteRouteModelList.add(routeModel);
        }
      });
      routeModelList = favouriteRouteModelList + nonFavouriteRouteModelList;
    }
    if (searchText != null) {
      routeModelList = routeModelList
          .where((e) => e.routeShortName.toLowerCase().contains(searchText.toLowerCase())
                || e.routeLongName.toLowerCase().contains(searchText.toLowerCase()))
          .toList();
    }
    return routeModelList;
  }

}
