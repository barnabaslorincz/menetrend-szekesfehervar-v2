import 'package:menetrend_szekesfehervar/models/stop_model.dart';
import 'package:menetrend_szekesfehervar/services/db/db_helper.dart';
import 'package:menetrend_szekesfehervar/services/favourites/favourites_service.dart';
import 'package:sqflite/sqflite.dart';

class StopDbService {

  final FavouritesService favouritesService = FavouritesService();

  Future<List<StopModel>> getAllStops({String? searchText}) async {
    final Database db = await DbHelper().initializeDb();
    final List<Map<String, Object?>> queryResult = await db.query('Stops', groupBy: 'stop_name', orderBy: 'stop_name ASC');
    List<StopModel> stopModelList = queryResult.map((e) => StopModel.fromMap(e)).toList();
    final List<String> favouriteStopNames = await favouritesService.getFavouriteStopNames();
    if (favouriteStopNames.isNotEmpty) {
      List<StopModel> favouriteStopModelList = [];
      List<StopModel> nonFavouriteStopModelList = [];
      stopModelList.forEach((stopModel) {
        if (favouriteStopNames.contains(stopModel.stopName)) {
          stopModel.isFavourite = true;
          favouriteStopModelList.add(stopModel);
        } else {
          nonFavouriteStopModelList.add(stopModel);
        }
      });
      stopModelList = favouriteStopModelList + nonFavouriteStopModelList;
    }
    if (searchText != null) {
      stopModelList = stopModelList
          .where((e) => e.stopName.toLowerCase().contains(searchText.toLowerCase()))
          .toList();
    }
    return stopModelList;
  }

}
