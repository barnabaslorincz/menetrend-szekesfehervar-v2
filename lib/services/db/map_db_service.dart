import 'package:menetrend_szekesfehervar/models/map_model.dart';
import 'package:sqflite/sqflite.dart';

import 'db_helper.dart';

class MapDbService {

  Future<List<MapModel>> getAllStops() async {
    final Database db = await DbHelper().initializeDb();
    final String queryString =
        "SELECT Stops.stop_id, Stops.stop_name, group_concat(DISTINCT route_short_name) as routes_at_stop, Stops.stop_lat, Stops.stop_lon"
        " from Stops"
        " inner join StopTimes on Stops.stop_id = StopTimes.stop_id"
        " inner join Trips on StopTimes.trip_id = Trips.trip_id"
        " inner join Routes"
        " where trips.route_id = routes.route_id"
        " group by Stops.stop_id";
    final List<Map<String, Object?>> queryResult = await db.rawQuery(queryString);
    return queryResult.map((e) => MapModel.fromMap(e)).toList();
  }

}
