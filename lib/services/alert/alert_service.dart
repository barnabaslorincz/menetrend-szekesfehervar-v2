import 'package:shared_preferences/shared_preferences.dart';

class AlertService {

  static const String SHOW_WELCOME_ALERT_KEY = "SHOW_WELCOME_ALERT";

  Future<bool> shouldShowWelcomeAlert() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    bool? shouldShowWelcomeAlert = sharedPreferences.getBool(SHOW_WELCOME_ALERT_KEY);
    if (shouldShowWelcomeAlert != null) {
      return shouldShowWelcomeAlert;
    }
    return true;
  }

  Future<void> onWelcomeAlertHidden() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool(SHOW_WELCOME_ALERT_KEY, false);
  }

}
