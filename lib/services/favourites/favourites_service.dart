import 'package:shared_preferences/shared_preferences.dart';

class FavouritesService {

  static const String FAVOURITE_ROUTE_IDS = "FAVOURITE_ROUTE_IDS";
  static const String FAVOURITE_STOP_NAMES = "FAVOURITE_STOP_NAMES";

  Future<List<String>> getFavouriteRouteIds() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    String? favouriteIdsString = sharedPreferences.getString(FAVOURITE_ROUTE_IDS);
    if (favouriteIdsString != null) {
      return favouriteIdsString.split(";").toList();
    }
    return List.empty();
  }

  Future<bool> addFavouriteRouteId(String routeId) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    String? favouriteIdsString = sharedPreferences.getString(FAVOURITE_ROUTE_IDS);
    if (favouriteIdsString != null) {
      List<String> favouriteRouteIds = favouriteIdsString.split(";").toList();
      if (!favouriteRouteIds.contains(routeId)) {
        favouriteRouteIds.add(routeId);
        return sharedPreferences.setString(FAVOURITE_ROUTE_IDS, favouriteRouteIds.join(';'));
      }
    } else {
      return sharedPreferences.setString(FAVOURITE_ROUTE_IDS, routeId);
    }
    return false;
  }

  Future<bool> removeFavouriteRouteId(String routeId) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    String? favouriteIdsString = sharedPreferences.getString(FAVOURITE_ROUTE_IDS);
    if (favouriteIdsString != null) {
      List<String> favouriteRouteIds = favouriteIdsString.split(";").toList();
      if (favouriteRouteIds.contains(routeId)) {
        favouriteRouteIds.remove(routeId);
        return sharedPreferences.setString(FAVOURITE_ROUTE_IDS, favouriteRouteIds.join(';'));
      }
    }
    return false;
  }

  Future<List<String>> getFavouriteStopNames() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    String? favouriteIdsString = sharedPreferences.getString(FAVOURITE_STOP_NAMES);
    if (favouriteIdsString != null) {
      return favouriteIdsString.split(";").toList();
    }
    return List.empty();
  }

  Future<bool> addFavouriteStopName(String routeId) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    String? favouriteIdsString = sharedPreferences.getString(FAVOURITE_STOP_NAMES);
    if (favouriteIdsString != null) {
      List<String> favouriteStopNames = favouriteIdsString.split(";").toList();
      if (!favouriteStopNames.contains(routeId)) {
        favouriteStopNames.add(routeId);
        return sharedPreferences.setString(FAVOURITE_STOP_NAMES, favouriteStopNames.join(';'));
      }
    } else {
      return sharedPreferences.setString(FAVOURITE_STOP_NAMES, routeId);
    }
    return false;
  }

  Future<bool> removeFavouriteStopName(String routeId) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    String? favouriteIdsString = sharedPreferences.getString(FAVOURITE_STOP_NAMES);
    if (favouriteIdsString != null) {
      List<String> favouriteStopNames = favouriteIdsString.split(";").toList();
      if (favouriteStopNames.contains(routeId)) {
        favouriteStopNames.remove(routeId);
        return sharedPreferences.setString(FAVOURITE_STOP_NAMES, favouriteStopNames.join(';'));
      }
    }
    return false;
  }

  Future<bool> isFavouriteStopName(stopId) async {
    if (stopId != null) {
      return await getFavouriteStopNames().then((favouriteIds) => favouriteIds.contains(stopId));
    }
    return false;
  }

}
