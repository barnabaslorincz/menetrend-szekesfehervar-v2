class RouteModel {
  final String routeId;
  final String routeShortName;
  final String routeLongName;
  final String routeType;
  final String routeColor;
  final String routeTextColor;
  bool isFavourite = false;

  RouteModel({
    required this.routeId,
    required this.routeShortName,
    required this.routeLongName,
    required this.routeType,
    required this.routeColor,
    required this.routeTextColor
  });

  RouteModel.fromMap(Map<String, dynamic> res):
        routeId = res["route_id"],
        routeShortName = res["route_short_name"],
        routeLongName = res["route_long_name"],
        routeType = res["route_type"],
        routeColor = res["route_color"],
        routeTextColor = res["route_text_color"];

  Map<String, Object?> toMap() {
    return {
      'route_id': routeId,
      'route_short_name': routeShortName,
      'route_long_name': routeLongName,
      'route_type': routeType,
      'route_color': routeColor,
      'route_text_color': routeTextColor,
    };
  }
}
