class StopModel {
  final String stopId;
  final String stopName;
  final double stopLatitude;
  final double stopLongitude;
  final String platformCode;
  bool isFavourite = false;

  StopModel({
    required this.stopId,
    required this.stopName,
    required this.stopLatitude,
    required this.stopLongitude,
    required this.platformCode,
  });

  StopModel.fromMap(Map<String, dynamic> res):
        stopId = res["stop_id"],
        stopName = res["stop_name"],
        stopLatitude = res["stop_lat"],
        stopLongitude = res["stop_lon"],
        platformCode = res["platform_code"];
}
