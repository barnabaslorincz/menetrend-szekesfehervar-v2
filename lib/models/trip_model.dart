import 'package:menetrend_szekesfehervar/models/trip_with_route_model.dart';

class TripModel {
  final String tripId;
  final String tripHeadsign;
  final String shapeId;
  final String departureTime;

  TripModel({
    required this.tripId,
    required this.tripHeadsign,
    required this.shapeId,
    required this.departureTime,
  });

  TripModel.fromMap(Map<String, dynamic> res):
        tripId = res["trip_id"],
        tripHeadsign = res["trip_headsign"],
        shapeId = res["shape_id"],
        departureTime = res["departure_time"];

  TripModel.fromTripModelWithRoute(TripModelWithRoute tripModelWithRoute):
        tripId = tripModelWithRoute.tripId,
        tripHeadsign = tripModelWithRoute.tripHeadsign,
        shapeId = "",
        departureTime = tripModelWithRoute.departureTime;

}
