class InfoModel {
  final String title;
  final String subtitle;
  final Function onTapAction;

  InfoModel(this.title, this.subtitle, this.onTapAction);
}