import 'package:menetrend_szekesfehervar/models/trip_model.dart';

class TripModelWithRoute {
  final String tripId;
  final String tripHeadsign;
  final String departureTime;
  final String routeShortName;

  TripModelWithRoute({
    required this.tripId,
    required this.tripHeadsign,
    required this.departureTime,
    required this.routeShortName
  });

  TripModelWithRoute.fromMap(Map<String, dynamic> res):
        tripId = res["trip_id"],
        tripHeadsign = res["trip_headsign"],
        departureTime = res["departure_time"],
        routeShortName = res["route_short_name"];

}
