class StopTimeModel {
  final String stopId;
  final String stopName;
  final String arrivalTime;
  final double stopLat;
  final double stopLon;

  StopTimeModel({
    required this.stopId,
    required this.stopName,
    required this.arrivalTime,
    required this.stopLat,
    required this.stopLon,
  });

  StopTimeModel.fromMap(Map<String, dynamic> res):
        stopId = res["stop_id"],
        stopName = res["stop_name"],
        arrivalTime = res["arrival_time"],
        stopLat = res["stop_lat"],
        stopLon = res["stop_lon"];

}
