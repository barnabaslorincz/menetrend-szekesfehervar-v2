class MapModel {
  final String stopId;
  final String stopName;
  final String routesAtStop;
  final double stopLat;
  final double stopLon;

  MapModel({
    required this.stopId,
    required this.stopName,
    required this.routesAtStop,
    required this.stopLat,
    required this.stopLon,
  });

  MapModel.fromMap(Map<String, dynamic> res):
        stopId = res["stop_id"],
        stopName = res["stop_name"],
        routesAtStop = res["routes_at_stop"].replaceAll(",", ", "),
        stopLat = res["stop_lat"],
        stopLon = res["stop_lon"];

}
