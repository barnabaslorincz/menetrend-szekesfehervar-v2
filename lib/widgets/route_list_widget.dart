import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:menetrend_szekesfehervar/models/route_model.dart';
import 'package:menetrend_szekesfehervar/screens/trip/trip_list_screen.dart';
import 'package:menetrend_szekesfehervar/services/db/route_db_service.dart';

class RouteListWidget extends StatefulWidget {
  final RouteDbService routeDbService = RouteDbService();
  final String? searchText;
  final Function searchCloseCallback;

  RouteListWidget({
    Key? key,
    this.searchText,
    required this.searchCloseCallback
  }) : super(key: key);

  @override
  RouteListWidgetState createState() => RouteListWidgetState();
}

class RouteListWidgetState extends State<RouteListWidget> {

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: widget.routeDbService.queryRoutes(searchText: widget.searchText),
      builder: (BuildContext context, AsyncSnapshot<List<RouteModel>> snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data?.length,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                  child: InkWell(
                      onTap: () {
                        // todo: decide if we want to close search
                        // after navigation to detail screen
                        // searchCloseCallback(context, "");
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => TripListScreen(
                              routeModel: snapshot.data![index],
                            ),
                          ),
                        ).then((value) => setState(() {}));
                      },
                      child: ListTile(
                        contentPadding: EdgeInsets.all(8.0),
                        title: Text(snapshot.data![index].routeShortName),
                        trailing: snapshot.data![index].isFavourite ? Icon(Icons.star, color: Colors.yellow) : null,
                        subtitle: Text(snapshot.data![index].routeLongName),
                      ))
              );
            },
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}