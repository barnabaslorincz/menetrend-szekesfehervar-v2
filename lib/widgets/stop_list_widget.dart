import 'package:flutter/material.dart';
import 'package:menetrend_szekesfehervar/models/stop_model.dart';
import 'package:menetrend_szekesfehervar/screens/stop/stop_detail_screen.dart';
import 'package:menetrend_szekesfehervar/screens/trip/trip_list_with_route_screen.dart';
import 'package:menetrend_szekesfehervar/services/db/stop_db_service.dart';

class StopListWidget extends StatefulWidget {
  final String? searchText;
  final Function searchCloseCallback;

  StopListWidget({
    Key? key,
    this.searchText,
    required this.searchCloseCallback
  }) : super(key: key);

  @override
  StopListWidgetState createState() => StopListWidgetState();

}

class StopListWidgetState extends State<StopListWidget> {
  final StopDbService stopDbService = StopDbService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: this.stopDbService.getAllStops(searchText: widget.searchText),
      builder: (BuildContext context, AsyncSnapshot<List<StopModel>> snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data?.length,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                  child: InkWell(
                      onTap: () {
                        // todo: decide if we want to close search
                        // after navigation to detail screen
                        // searchCloseCallback(context, "");
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => TripListWithRouteScreen(
                              stopName: snapshot.data![index].stopName
                            ),
                          ),
                        ).then((value) => setState(() {}));
                      },
                      child: ListTile(
                        contentPadding: EdgeInsets.all(8.0),
                        title: Text(snapshot.data![index].stopName),
                        trailing: snapshot.data![index].isFavourite ? Icon(Icons.star, color: Colors.yellow) : null,
                      ))
              );
            },
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
