import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:menetrend_szekesfehervar/models/stop_time_model.dart';
import 'package:menetrend_szekesfehervar/models/trip_model.dart';
import 'package:menetrend_szekesfehervar/screens/stop_time/stop_time_list_screen.dart';
import 'package:menetrend_szekesfehervar/screens/stop_time/stop_time_map_screen.dart';
import 'package:menetrend_szekesfehervar/services/db/stop_time_db_service.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TripDetailScreen extends StatefulWidget {
  final TripModel tripModel;
  final String routeShortName;

  TripDetailScreen({Key? key, required this.tripModel, required this.routeShortName}) : super(key: key);

  @override
  _TripDetailScreenState createState() => _TripDetailScreenState();
}

class _TripDetailScreenState extends State<TripDetailScreen> {
  StopTimeDbService stopTimeDbService = StopTimeDbService();

  @override
  Widget build(BuildContext context) {
    return
      DefaultTabController(length: 2, child:
      Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.timer), text: AppLocalizations.of(context)!.times),
              Tab(icon: Icon(Icons.map), text: AppLocalizations.of(context)!.map),
            ],
          ),
          title: Text(widget.routeShortName + " - " + widget.tripModel.tripHeadsign),
        ),
        body: FutureBuilder(
        future: this.stopTimeDbService.queryStopTimesForTrip(widget.tripModel.tripId),
          builder: (BuildContext context, AsyncSnapshot<List<StopTimeModel>> snapshot) {
            if (snapshot.hasData) {
              return TabBarView(
                children: [
                  StopTimeListScreen(stopTimeModelList: snapshot),
                  StopTimeMapScreen(stopTimeModelList: snapshot),
                ],
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
         ),
        ),
      );
  }
}