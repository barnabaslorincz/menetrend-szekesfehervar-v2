import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:menetrend_szekesfehervar/models/route_model.dart';
import 'package:menetrend_szekesfehervar/models/trip_model.dart';
import 'package:menetrend_szekesfehervar/models/trip_with_route_model.dart';
import 'package:menetrend_szekesfehervar/screens/trip/trip_detail_screen.dart';
import 'package:menetrend_szekesfehervar/services/db/trip_db_service.dart';
import 'package:menetrend_szekesfehervar/services/favourites/favourites_service.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TripListWithRouteScreen extends StatefulWidget {
  final String? stopId;
  final String stopName;

  TripListWithRouteScreen({Key? key, this.stopId, required this.stopName}) : super(key: key);

@override
_TripListWithRouteScreenState createState() => _TripListWithRouteScreenState(stopId, stopName);
}

class _TripListWithRouteScreenState extends State<TripListWithRouteScreen> {

  TripDbService tripDbService = TripDbService();
  FavouritesService favouritesService = FavouritesService();
  DateTime selectedDate = DateTime.now();
  bool isFavourite = false;

  _TripListWithRouteScreenState(stopId, stopName) {
    if (stopId == null) {
      favouritesService.isFavouriteStopName(stopName).then((isFavourite) {
        this.isFavourite = isFavourite;
        setState(() {});
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.stopName),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.calendar_today,
              color: Colors.white,
            ),
            onPressed: () => _selectDate(context),
          ),
          if (widget.stopId == null) IconButton(
            icon: Icon(
              isFavourite ? Icons.star : Icons.star_border,
              color: Colors.yellow,
            ),
            onPressed: () => _toggleFavourite(),
          )
        ],
      ),
        body: Column(
          children: [
            Container(
              color: Colors.blue,
              child: ListTile(
                onTap: () => _selectDate(context),
                title: Text(getDateText(context)),
              ),
            ),
            Expanded(
              child: FutureBuilder(
                future: widget.stopId != null ? this.tripDbService.queryTripsForStopIdWithDate(widget.stopId!, selectedDate)
                    : this.tripDbService.queryTripsForStopNameWithDate(widget.stopName, selectedDate),
                builder: (BuildContext context, AsyncSnapshot<List<TripModelWithRoute>> snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data?.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                            child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TripDetailScreen(
                                        routeShortName: snapshot.data![index].routeShortName,
                                        tripModel: TripModel.fromTripModelWithRoute(snapshot.data![index]),
                                      ),
                                    ),
                                  );
                                },
                                child: ListTile(
                                  contentPadding: EdgeInsets.all(8.0),
                                  title: Text(snapshot.data![index].routeShortName + " - " + snapshot.data![index].tripHeadsign),
                                  subtitle: Text(snapshot.data![index].departureTime),
                                ))
                        );
                      },
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
          ],
        )
    );
  }

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      locale : Localizations.localeOf(context),
      initialDate: selectedDate,
      firstDate: DateTime(2022, 1, 1),
      lastDate: DateTime(2022, 12, 31),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  String getTodayText() {
    DateTime currentDate = DateTime.now();
    return selectedDate.day == currentDate.day
        && selectedDate.month == currentDate.month
        ? "(" + AppLocalizations.of(context)!.today + ")" : "";
  }

  _toggleFavourite() {
    if (widget.stopId == null) {
      if (isFavourite) {
        favouritesService.removeFavouriteStopName(widget.stopName)
            .whenComplete(() => setState(() => {
          isFavourite = false
        }));
      } else {
        favouritesService.addFavouriteStopName(widget.stopName)
            .whenComplete(() => setState(() => {
          isFavourite = true
        }));
      }
    }
  }

  String getDateText(BuildContext context) {
    DateFormat dateFormat = new DateFormat('y. MMMM d., EEEE', Localizations.localeOf(context).languageCode);
    return dateFormat.format(selectedDate) + " " + getTodayText();
  }
}