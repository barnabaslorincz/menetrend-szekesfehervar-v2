import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StopDetailScreen extends StatelessWidget {
  final String stopName;

  StopDetailScreen({Key? key, required this.stopName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return 
      DefaultTabController(length: 2, child:
      Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.directions_car)),
              Tab(icon: Icon(Icons.directions_transit)),
            ],
          ),
          title: Text(stopName),
        ),
        body: TabBarView(
          children: [
            Icon(Icons.directions_car),
            Icon(Icons.directions_transit),
          ],
        ),
      ),
    );
  }
}