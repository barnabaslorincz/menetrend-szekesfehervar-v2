import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:menetrend_szekesfehervar/search/stop_search.dart';
import 'package:menetrend_szekesfehervar/widgets/stop_list_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class StopListScreen extends StatefulWidget {
  @override
  _StopListScreenState createState() => _StopListScreenState();
}

class _StopListScreenState extends State<StopListScreen> {
  StopSearchDelegate stopSearchDelegate = StopSearchDelegate();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.stops),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () async {
              showSearch(context: context, delegate: stopSearchDelegate);
            },
          )
        ],
      ),
      body: StopListWidget(searchCloseCallback: stopSearchDelegate.close)
    );
  }
}