import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:menetrend_szekesfehervar/screens/info/info_screen.dart';
import 'package:menetrend_szekesfehervar/screens/map/map_screen.dart';
import 'package:menetrend_szekesfehervar/screens/route/route_list_screen.dart';
import 'package:menetrend_szekesfehervar/screens/stop/stop_list_screen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:menetrend_szekesfehervar/services/alert/alert_service.dart';

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  int _selectedIndex = 0;
  AlertService alertService = AlertService();

  final _screens = [
    MapScreen(),
    StopListScreen(),
    RouteListScreen(),
    InfoScreen(),
  ];

  @override
  void initState() {
    super.initState();
    displayWelcomeAlert();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
          children: _screens.asMap().map((i, screen) => MapEntry(
                    i,
                    Offstage(offstage: _selectedIndex != i,child: screen)
                  )
          ).values.toList()
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: (i) => setState(() => _selectedIndex = i),
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 12,
        unselectedFontSize: 12,
        selectedItemColor: Colors.blue,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.map_outlined),
            activeIcon: Icon(Icons.map),
            label: AppLocalizations.of(context)!.map
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.hail_outlined),
            activeIcon: Icon(Icons.hail),
            label: AppLocalizations.of(context)!.stops
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.directions_bus_outlined),
            activeIcon: Icon(Icons.directions_bus),
            label: AppLocalizations.of(context)!.routes
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.info_outline),
            activeIcon: Icon(Icons.info),
            label: AppLocalizations.of(context)!.info
          ),
      ],),
    );
  }

  void displayWelcomeAlert() {
    alertService.shouldShowWelcomeAlert().then((shouldShowWelcomeAlert) {
      if (shouldShowWelcomeAlert) {
        WidgetsBinding.instance!.addPostFrameCallback((_) async {
          await showDialog<String>(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) => new AlertDialog(
              title: new Text(AppLocalizations.of(context)!.welcomeTitle),
              content: new Text(AppLocalizations.of(context)!.welcomeMessage),
              actions: <Widget>[
                new TextButton(
                  child: new Text(AppLocalizations.of(context)!.welcomeButton),
                  onPressed: () {
                    Navigator.of(context).pop();
                    alertService.onWelcomeAlertHidden();
                  },
                ),
              ],
            ),
          );
        });
      }
    });
  }
}