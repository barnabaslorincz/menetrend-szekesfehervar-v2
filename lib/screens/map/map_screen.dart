import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:menetrend_szekesfehervar/models/map_model.dart';
import 'package:menetrend_szekesfehervar/screens/trip/trip_list_with_route_screen.dart';
import 'package:menetrend_szekesfehervar/services/db/map_db_service.dart';
import 'package:menetrend_szekesfehervar/utils/map_utils.dart';

class MapScreen extends StatefulWidget {

  MapScreen({Key? key}) : super(key: key);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  static const _initialCameraPosition = CameraPosition(
      target: LatLng(47.1887601, 18.409238),
      zoom: 11
  );

  BitmapDescriptor markerIcon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue);
  BitmapDescriptor smallMarkerIcon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue);
  BitmapDescriptor largeMarkerIcon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue);

  Completer<GoogleMapController> _controller = Completer();

  Set<MapModel> mapModels = Set();

  @override
  void initState() {
    super.initState();
  }

  @override
  Future<void> dispose() async {
    super.dispose();
    final GoogleMapController controller = await _controller.future;
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GoogleMap(
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
            createMarkers().then((value) {
              setState(() {
                controller.animateCamera(CameraUpdate.newLatLngBounds(
                    MapUtils.boundsFromLatLngList(mapModels.map((e) => new LatLng(e.stopLat, e.stopLon)).toList()), 50));
              });
            });
        },
          gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
            new Factory<OneSequenceGestureRecognizer>(() => new EagerGestureRecognizer())
          ].toSet(),
          initialCameraPosition: _initialCameraPosition,
          markers: mapModels.map((mapModel) {
            return Marker(
              markerId: MarkerId(mapModel.stopId),
              infoWindow: InfoWindow(
                  title: mapModel.stopName,
                  snippet: mapModel.routesAtStop,
                  onTap:() {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TripListWithRouteScreen(
                            stopName: mapModel.stopName,
                            stopId: mapModel.stopId
                        ),
                      ),
                    );
                  }
              ),
              icon: markerIcon,
              position: new LatLng(mapModel.stopLat, mapModel.stopLon),
            );
          }).toSet(),
          onCameraIdle: () {
            setState(() {
            });
          },
          onCameraMove:(CameraPosition cameraPosition) {
            cameraPosition.zoom > 14 ?
              markerIcon = largeMarkerIcon
            : markerIcon = smallMarkerIcon;
          },
        )
    );
  }
  Future<void> createMarkers() async {
    await MapDbService().getAllStops().then((mapModels) => {
      this.mapModels = mapModels.toSet()
    });
    final ImageConfiguration imageConfiguration =
    createLocalImageConfiguration(context, size: Size.square(24));
    await BitmapDescriptor.fromAssetImage(
        imageConfiguration, 'assets/markers/marker.png').then((value) => largeMarkerIcon = value);
    await BitmapDescriptor.fromAssetImage(
        imageConfiguration, 'assets/markers/dot.png').then((value) => smallMarkerIcon = value);
  }

}