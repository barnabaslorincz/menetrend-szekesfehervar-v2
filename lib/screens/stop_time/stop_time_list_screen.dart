import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:menetrend_szekesfehervar/models/stop_time_model.dart';
import 'package:menetrend_szekesfehervar/services/db/route_db_service.dart';

class StopTimeListScreen extends StatefulWidget {
  final AsyncSnapshot<List<StopTimeModel>> stopTimeModelList;

  StopTimeListScreen({Key? key, required this.stopTimeModelList}) : super(key: key);

  @override
  _StopTimeListScreenState createState() => _StopTimeListScreenState();
}

class _StopTimeListScreenState extends State<StopTimeListScreen> {
  final String title = "StopTimeList";
  RouteDbService routeDbService = RouteDbService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
              itemCount: widget.stopTimeModelList.data?.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                    child: InkWell(
                        onTap: () {
                          // Show map
                        },
                        child: ListTile(
                          contentPadding: EdgeInsets.all(8.0),
                          title: Text(widget.stopTimeModelList.data![index].stopName),
                          subtitle: Text(widget.stopTimeModelList.data![index].arrivalTime),
                        ))
                );
              },
            ),
    );
  }
}