import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:menetrend_szekesfehervar/models/stop_time_model.dart';
import 'package:menetrend_szekesfehervar/utils/map_utils.dart';

class StopTimeMapScreen extends StatefulWidget {
  final AsyncSnapshot<List<StopTimeModel>> stopTimeModelList;

  StopTimeMapScreen({Key? key, required this.stopTimeModelList}) : super(key: key);

  @override
  _StopTimeMapScreenState createState() => _StopTimeMapScreenState();
}

class _StopTimeMapScreenState extends State<StopTimeMapScreen> {
  static const _initialCameraPosition = CameraPosition(
      target: LatLng(47.1887601, 18.409238),
      zoom: 11
  );

  Set<Marker> markers = {};

  Completer<GoogleMapController> _controller = Completer();

  @override
  Future<void> dispose() async {
    super.dispose();
    final GoogleMapController controller = await _controller.future;
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
          createMarkers();
          WidgetsBinding.instance!.addPostFrameCallback((_) {
            controller.animateCamera(CameraUpdate.newLatLngBounds(
                MapUtils.boundsFromLatLngList(markers.map((e) => e.position).toList()), 50));
          });
          setState(() {});
        },
        gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
          new Factory<OneSequenceGestureRecognizer>(() => new EagerGestureRecognizer())
        ].toSet(),
        initialCameraPosition: _initialCameraPosition,
        markers: markers,
      )
    );
  }

  void createMarkers() {
    markers = widget.stopTimeModelList.data!.map((marker) => Marker(
      markerId: MarkerId(marker.stopId),
      infoWindow: InfoWindow(title: marker.stopName, snippet: marker.arrivalTime),
      icon:
      BitmapDescriptor.defaultMarkerWithHue(_getMarkerIconColor(marker.stopId)),
      position: LatLng(marker.stopLat, marker.stopLon))
    ).toSet();
  }

  double _getMarkerIconColor(String stopId) {
    if (
        widget.stopTimeModelList.data!.length > 0
          && widget.stopTimeModelList.data![0].stopId == stopId) {
      return BitmapDescriptor.hueGreen;
    } else if (
      widget.stopTimeModelList.data!.length > 0
          && widget.stopTimeModelList.data!.last.stopId == stopId
    ) {
      return BitmapDescriptor.hueRed;
    }
    return BitmapDescriptor.hueBlue;
  }
}