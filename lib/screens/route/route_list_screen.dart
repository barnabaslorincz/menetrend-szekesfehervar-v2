import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:menetrend_szekesfehervar/search/route_search.dart';
import 'package:menetrend_szekesfehervar/widgets/route_list_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class RouteListScreen extends StatefulWidget {
  @override
  _RouteListScreenState createState() => _RouteListScreenState();
}

class _RouteListScreenState extends State<RouteListScreen> {

  @override
  Widget build(BuildContext context) {
    RouteSearchDelegate routeSearchDelegate = RouteSearchDelegate(setStateCallback: setState);
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.routes),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () async {
              showSearch(context: context, delegate: routeSearchDelegate);
            },
          )
        ],
      ),
      body: RouteListWidget(searchCloseCallback: routeSearchDelegate.close)
    );
  }
}