import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RouteDetailScreen extends StatelessWidget {
  final String routeShortName;

  RouteDetailScreen({Key? key, required this.routeShortName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return 
      DefaultTabController(length: 2, child:
      Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.directions_car)),
              Tab(icon: Icon(Icons.directions_transit)),
            ],
          ),
          title: Text(routeShortName),
        ),
        body: TabBarView(
          children: [
            Icon(Icons.directions_car),
            Icon(Icons.directions_transit),
          ],
        ),
      ),
    );
  }
}