import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:menetrend_szekesfehervar/constants/constants.dart' as Constants;
import 'package:menetrend_szekesfehervar/models/info_model.dart';
import 'package:url_launcher/url_launcher.dart';

class InfoScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    List<InfoModel> infoModelList = [
      new InfoModel(
          AppLocalizations.of(context)!.cityWebsite,
          Constants.CITY_WEBSITE,
          goToCityWebsite
      ),
      new InfoModel(
          AppLocalizations.of(context)!.volanbuszWebsite,
          Constants.VOLANBUSZ_WEBSITE,
          goToVolanbuszWebsite
      ),
      new InfoModel(
          AppLocalizations.of(context)!.appVersion,
          Constants.APP_VERSION,
          () {}
      ),
      new InfoModel(
          AppLocalizations.of(context)!.gtfsVersion,
          Constants.GTFS_VERSION,
          () {}
      ),
      new InfoModel(
          AppLocalizations.of(context)!.lastUpdated,
          getLastUpdatedText(context, Constants.LAST_UPDATED),
          () {}
      )
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.info),
      ),
      body: ListView.builder(
        itemCount: infoModelList.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
              child: InkWell(
                  onTap: () {
                    infoModelList[index].onTapAction();
                  },
                  child: ListTile(
                    contentPadding: EdgeInsets.all(8.0),
                    title: Text(infoModelList[index].title),
                    subtitle: Text(infoModelList[index].subtitle),
                  ))
          );
        },
      ),
    );
  }

  void launchUrl(String url) async {
    await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
  }

  goToCityWebsite() {
    launchUrl(Constants.CITY_WEBSITE);
  }

  goToVolanbuszWebsite() {
    launchUrl(Constants.VOLANBUSZ_WEBSITE);
  }

  String getLastUpdatedText(BuildContext context, String lastUpdated) {
    final dateTime = DateTime.parse(lastUpdated);
    DateFormat dateFormat = new DateFormat('y. MMMM d.', Localizations.localeOf(context).languageCode);
    return dateFormat.format(dateTime);
  }

}
