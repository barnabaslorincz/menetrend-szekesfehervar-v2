import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapUtils {

  static LatLngBounds boundsFromLatLngList(List<LatLng> list) {
    assert(list.isNotEmpty);
    double? x0, x1, y0, y1;
    for (LatLng latLng in list) {
      if (x0 == null) {
        x0 = x1 = latLng.latitude;
        y0 = y1 = latLng.longitude;
      } else {
        if (x1 != null && latLng.latitude > x1) x1 = latLng.latitude;
        if (x0 != null && latLng.latitude < x0) x0 = latLng.latitude;
        if (y1 != null && latLng.longitude > y1) y1 = latLng.longitude;
        if (y0 != null && latLng.longitude < y0) y0 = latLng.longitude;
      }
    }
    if (
    x1 != null
        && x0 != null
        && y1 != null
        && y0 != null
    ) {
      return LatLngBounds(northeast: LatLng(x1, y1), southwest: LatLng(x0, y0));
    }
    return LatLngBounds(northeast: LatLng(0, 0), southwest: LatLng(0, 0));
  }

}
