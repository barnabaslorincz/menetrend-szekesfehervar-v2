import 'package:flutter/material.dart';
import 'package:menetrend_szekesfehervar/widgets/stop_list_widget.dart';

class StopSearchDelegate extends SearchDelegate<String> {

  @override
  List<Widget> buildActions(BuildContext context) => [
    IconButton(
      icon: Icon(Icons.clear),
      onPressed: () {
        if (query.isEmpty) {
          close(context, query);
        } else {
          query = '';
          showSuggestions(context);
        }
      },
    )
  ];

  @override
  Widget buildLeading(BuildContext context) => IconButton(
    icon: Icon(Icons.arrow_back),
    onPressed: () => close(context, query),
  );

  @override
  Widget buildResults(BuildContext context) =>
      StopListWidget(searchText: query, searchCloseCallback: close);

  @override
  Widget buildSuggestions(BuildContext context) =>
      StopListWidget(searchText: query, searchCloseCallback: close);
}
