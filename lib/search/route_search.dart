import 'package:flutter/material.dart';
import 'package:menetrend_szekesfehervar/widgets/route_list_widget.dart';

class RouteSearchDelegate extends SearchDelegate<String> {
  Function setStateCallback;

  RouteSearchDelegate({required this.setStateCallback});

  @override
  List<Widget> buildActions(BuildContext context) => [
    IconButton(
      icon: Icon(Icons.clear),
      onPressed: () {
        if (query.isEmpty) {
          close(context, query);
        } else {
          query = '';
          showSuggestions(context);
        }
      },
    )
  ];

  @override
  Widget buildLeading(BuildContext context) => IconButton(
    icon: Icon(Icons.arrow_back),
    onPressed: () {
      setStateCallback(() {});
      close(context, query);
    }
  );

  @override
  Widget buildResults(BuildContext context) => 
      RouteListWidget(searchText: query, searchCloseCallback: close);

  @override
  Widget buildSuggestions(BuildContext context) =>
      RouteListWidget(searchText: query, searchCloseCallback: close);
}