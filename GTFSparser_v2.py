import json

sqlLines = []
#
# sqlLines.append("CREATE TABLE FeedInfo;")
# sqlLines.append("\n") nem kell
# sqlLines.append("CREATE TABLE Agency;")
# sqlLines.append("\n") nem kell
#

sqlLines.append("DROP TABLE IF EXISTS CalendarDates;")
sqlLines.append("\n")


sqlLines.append("CREATE TABLE CalendarDates(service_id VARCHAR(255), date DATE, exception_type INTEGER);")
sqlLines.append("\n")

databaseName = "CalendarDates"
fileName = "calendar_dates.txt"
structure = [
    {
        "nameInDatabase": "service_id",
        "type": "string",
        "indexInDatabase": 0,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "date",
        "type": "date",
        "indexInDatabase": 1,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "exception_type",
        "type": "integer",
        "indexInDatabase": 2,
        "indexInGTFS": None,
    }
]

i = 1
with open(fileName) as my_filex:
    for line in my_filex:
        if i == 1:
            headers = line.replace("\n", "").split(",")
            for headerIndex, header in enumerate(headers):
                for structureItemIndex, structureItem in enumerate(structure):
                    if header == structureItem["nameInDatabase"]:
                        structureItem["indexInGTFS"] = headerIndex
        else:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            values = line.replace("\n", "").split(",")
            sqlValues = []
            for structureItemIndex, structureItem in enumerate(structure):
                sqlValue = values[structureItem["indexInGTFS"]]
                if structureItem["type"] == "string":
                    sqlValues.append("\"" + sqlValue + "\"")
                else:
                    sqlValues.append(sqlValue)

            sqlLines.append("INSERT INTO " + databaseName + " VALUES (" + ",".join(sqlValues) + ");")
            sqlLines.append("\n")
        i = i+1

sqlLines.append("DROP TABLE IF EXISTS Calendar;")
sqlLines.append("\n")

sqlLines.append("CREATE TABLE Calendar(service_id VARCHAR(255), monday INTEGER, tuesday INTEGER, wednesday INTEGER, thursday INTEGER, friday INTEGER, saturday INTEGER, sunday INTEGER, start_date DATE, end_date DATE);")
sqlLines.append("\n")


databaseName = "Calendar"
fileName = "calendar.txt"
structure = [
    {
        "nameInDatabase": "service_id",
        "type": "string",
        "indexInDatabase": 0,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "monday",
        "type": "integer",
        "indexInDatabase": 1,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "tuesday",
        "type": "integer",
        "indexInDatabase": 2,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "wednesday",
        "type": "integer",
        "indexInDatabase": 3,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "thursday",
        "type": "integer",
        "indexInDatabase": 4,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "friday",
        "type": "integer",
        "indexInDatabase": 5,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "saturday",
        "type": "integer",
        "indexInDatabase": 6,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "sunday",
        "type": "integer",
        "indexInDatabase": 7,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "start_date",
        "type": "date",
        "indexInDatabase": 8,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "end_date",
        "type": "date",
        "indexInDatabase": 9,
        "indexInGTFS": None,
    }
]

i = 1
with open(fileName) as my_filex:
    for line in my_filex:
        if i == 1:
            headers = line.replace("\n", "").split(",")
            for headerIndex, header in enumerate(headers):
                for structureItemIndex, structureItem in enumerate(structure):
                    if header == structureItem["nameInDatabase"]:
                        structureItem["indexInGTFS"] = headerIndex
        else:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            values = line.replace("\n", "").split(",")
            sqlValues = []
            for structureItemIndex, structureItem in enumerate(structure):
                sqlValue = values[structureItem["indexInGTFS"]]
                if structureItem["type"] == "string":
                    sqlValues.append("\"" + sqlValue + "\"")
                else:
                    sqlValues.append(sqlValue)

            sqlLines.append("INSERT INTO " + databaseName + " VALUES (" + ",".join(sqlValues) + ");")
            sqlLines.append("\n")
        i = i+1

sqlLines.append("DROP TABLE IF EXISTS Routes;")
sqlLines.append("\n")
#
sqlLines.append("CREATE TABLE Routes(route_id VARCHAR(255), route_short_name VARCHAR(255), route_long_name VARCHAR(255), route_type VARCHAR(255), route_color VARCHAR(255), route_text_color VARCHAR(255));")
sqlLines.append("\n")

databaseName = "Routes"
fileName = "routes.txt"

structure = [
    {
        "nameInDatabase": "route_id",
        "type": "string",
        "indexInDatabase": 0,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "route_short_name",
        "type": "string",
        "indexInDatabase": 1,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "route_long_name",
        "type": "string",
        "indexInDatabase": 2,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "route_type",
        "type": "string",
        "indexInDatabase": 3,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "route_color",
        "type": "string",
        "indexInDatabase": 4,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "route_text_color",
        "type": "string",
        "indexInDatabase": 5,
        "indexInGTFS": None,
    }
]

i = 1
with open(fileName) as my_filex:
    for line in my_filex:
        if i == 1:
            headers = line.replace("\n", "").split(",")
            for headerIndex, header in enumerate(headers):
                for structureItemIndex, structureItem in enumerate(structure):
                    if header == structureItem["nameInDatabase"]:
                        structureItem["indexInGTFS"] = headerIndex
        else:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            values = line.replace("\n", "").split(",")
            sqlValues = []
            for structureItemIndex, structureItem in enumerate(structure):
                sqlValue = values[structureItem["indexInGTFS"]]
                if structureItem["type"] == "string":
                    sqlValues.append("\"" + sqlValue + "\"")
                else:
                    sqlValues.append(sqlValue)

            sqlLines.append("INSERT INTO " + databaseName + " VALUES (" + ",".join(sqlValues) + ");")
            sqlLines.append("\n")
        i = i+1

sqlLines.append("DROP TABLE IF EXISTS Shapes;")
sqlLines.append("\n")
#
sqlLines.append("CREATE TABLE Shapes(shape_id VARCHAR(255), shape_pt_sequence INTEGER, shape_pt_lat REAL, shape_pt_lon REAL);")
sqlLines.append("\n")

databaseName = "Shapes"
fileName = "shapes.txt"
structure = [
    {
        "nameInDatabase": "shape_id",
        "type": "string",
        "indexInDatabase": 0,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "shape_pt_sequence",
        "type": "integer",
        "indexInDatabase": 1,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "shape_pt_lat",
        "type": "real",
        "indexInDatabase": 2,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "shape_pt_lon",
        "type": "real",
        "indexInDatabase": 3,
        "indexInGTFS": None,
    }
]

i = 1
with open(fileName) as my_filex:
    for line in my_filex:
        if i == 1:
            headers = line.replace("\n", "").split(",")
            for headerIndex, header in enumerate(headers):
                for structureItemIndex, structureItem in enumerate(structure):
                    if header == structureItem["nameInDatabase"]:
                        structureItem["indexInGTFS"] = headerIndex
        else:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            values = line.replace("\n", "").split(",")
            sqlValues = []
            for structureItemIndex, structureItem in enumerate(structure):
                sqlValue = values[structureItem["indexInGTFS"]]
                if structureItem["type"] == "string":
                    sqlValues.append("\"" + sqlValue + "\"")
                else:
                    sqlValues.append(sqlValue)

            sqlLines.append("INSERT INTO " + databaseName + " VALUES (" + ",".join(sqlValues) + ");")
            sqlLines.append("\n")
        i = i+1

sqlLines.append("DROP TABLE IF EXISTS StopTimes;")
sqlLines.append("\n")

sqlLines.append("CREATE TABLE StopTimes(trip_id VARCHAR(255), stop_sequence INTEGER, stop_id VARCHAR(255), arrival_time TIME, departure_time TIME, stop_headsign VARCHAR(255));")
sqlLines.append("\n")


databaseName = "StopTimes"
fileName = "stop_times.txt"
structure = [
    {
        "nameInDatabase": "trip_id",
        "type": "string",
        "indexInDatabase": 0,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "stop_sequence",
        "type": "integer",
        "indexInDatabase": 1,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "stop_id",
        "type": "string",
        "indexInDatabase": 2,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "arrival_time",
        "type": "string",
        "indexInDatabase": 3,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "departure_time",
        "type": "string",
        "indexInDatabase": 4,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "stop_headsign",
        "type": "string",
        "indexInDatabase": 5,
        "indexInGTFS": None,
    }
]

i = 1
with open(fileName) as my_filex:
    for line in my_filex:
        if i == 1:
            headers = line.replace("\n", "").split(",")
            for headerIndex, header in enumerate(headers):
                for structureItemIndex, structureItem in enumerate(structure):
                    if header == structureItem["nameInDatabase"]:
                        structureItem["indexInGTFS"] = headerIndex
        else:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            values = line.replace("\n", "").split(",")
            sqlValues = []
            for structureItemIndex, structureItem in enumerate(structure):
                sqlValue = values[structureItem["indexInGTFS"]]
                if structureItem["type"] == "string":
                    sqlValues.append("\"" + sqlValue + "\"")
                else:
                    sqlValues.append(sqlValue)

            sqlLines.append("INSERT INTO " + databaseName + " VALUES (" + ",".join(sqlValues) + ");")
            sqlLines.append("\n")
        i = i+1

sqlLines.append("DROP TABLE IF EXISTS Stops;")
sqlLines.append("\n")
#
sqlLines.append("CREATE TABLE Stops(stop_id VARCHAR(255), stop_name VARCHAR(255), stop_desc VARCHAR(255), stop_lat REAL, stop_lon REAL, platform_code VARCHAR(255));")
sqlLines.append("\n")


databaseName = "Stops"
fileName = "stops.txt"
structure = [
    {
        "nameInDatabase": "stop_id",
        "type": "string",
        "indexInDatabase": 0,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "stop_name",
        "type": "string",
        "indexInDatabase": 1,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "stop_desc",
        "type": "string",
        "indexInDatabase": 2,
        "indexInGTFS": None,
        "defaultValue": "",
    },
    {
        "nameInDatabase": "stop_lat",
        "type": "real",
        "indexInDatabase": 3,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "stop_lon",
        "type": "real",
        "indexInDatabase": 4,
        "indexInGTFS": None,
    }
]

i = 1
with open(fileName) as my_filex:
    for line in my_filex:
        if i == 1:
            headers = line.replace("\n", "").split(",")
            for headerIndex, header in enumerate(headers):
                for structureItemIndex, structureItem in enumerate(structure):
                    if header == structureItem["nameInDatabase"]:
                        structureItem["indexInGTFS"] = headerIndex
        else:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            values = line.replace("\n", "").split(",")
            sqlValues = []
            for structureItemIndex, structureItem in enumerate(structure):
                if structureItem["indexInGTFS"] is None:
                    sqlValues.append("\"" + structureItem["defaultValue"] + "\"")
                else:
                    sqlValue = values[structureItem["indexInGTFS"]]
                    if structureItem["type"] == "string":
                        sqlValues.append("\"" + sqlValue + "\"")
                    else:
                        sqlValues.append(sqlValue)

            sqlLines.append("INSERT INTO " + databaseName + " VALUES (" + ",".join(sqlValues) + ", \"0\");")
            sqlLines.append("\n")
        i = i+1

sqlLines.append("DROP TABLE IF EXISTS Trips;")
sqlLines.append("\n")
#
sqlLines.append("CREATE TABLE Trips(trip_id VARCHAR(255), route_id VARCHAR(255), service_id VARCHAR(255), trip_headsign VARCHAR(255), shape_id VARCHAR(255));")
sqlLines.append("\n")


databaseName = "Trips"
fileName = "trips.txt"
structure = [
    {
        "nameInDatabase": "trip_id",
        "type": "string",
        "indexInDatabase": 0,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "route_id",
        "type": "string",
        "indexInDatabase": 1,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "service_id",
        "type": "string",
        "indexInDatabase": 2,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "trip_headsign",
        "type": "string",
        "indexInDatabase": 3,
        "indexInGTFS": None,
    },
    {
        "nameInDatabase": "shape_id",
        "type": "string",
        "indexInDatabase": 4,
        "indexInGTFS": None,
    }
]

i = 1
with open(fileName) as my_filex:
    for line in my_filex:
        if i == 1:
            headers = line.replace("\n", "").split(",")
            for headerIndex, header in enumerate(headers):
                for structureItemIndex, structureItem in enumerate(structure):
                    if header == structureItem["nameInDatabase"]:
                        structureItem["indexInGTFS"] = headerIndex
        else:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            values = line.replace("\n", "").split(",")
            sqlValues = []
            for structureItemIndex, structureItem in enumerate(structure):
                sqlValue = values[structureItem["indexInGTFS"]]
                if structureItem["type"] == "string":
                    sqlValues.append("\"" + sqlValue + "\"")
                else:
                    sqlValues.append(sqlValue)

            sqlLines.append("INSERT INTO " + databaseName + " VALUES (" + ",".join(sqlValues) + ");")
            sqlLines.append("\n")
        i = i+1


file1 = open("insert.sql","w")

file1.writelines(sqlLines)
file1.close()
